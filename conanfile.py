from conans import ConanFile, AutoToolsBuildEnvironment, VisualStudioBuildEnvironment, tools
import os
import shutil
import glob

#$(eval $(call gb_ExternalProject_ExternalProject,libgpg-error))
#
#$(eval $(call gb_ExternalProject_register_targets,libgpg-error,\
#	build \
#))
#
#$(eval $(call gb_ExternalProject_use_autoconf,libgpg-error,build))
#
#ifeq ($(COM),MSC)
#gb_ExternalProject_libgpg-error_host := $(if $(filter INTEL,$(CPUNAME)),i686-mingw32,x86_64-w64-mingw32)
#gb_ExternalProject_libgpg-error_target := $(if $(filter INTEL,$(CPUNAME)),pe-i386,pe-x86-64)
#$(call gb_ExternalProject_get_state_target,libgpg-error,build): $(call gb_Executable_get_target,cpp)
#	$(call gb_Trace_StartRange,libgpg-error,EXTERNAL)
#	$(call gb_ExternalProject_run,build,\
#		MAKE=$(MAKE) ./configure \
#			--enable-static \
#			--disable-shared \
#			--disable-rpath \
#			--disable-languages \
#			--disable-doc \
#			--disable-tests \
#			--host=$(gb_ExternalProject_libgpg-error_host) \
#			RC='windres -O COFF --target=$(gb_ExternalProject_libgpg-error_target) --preprocessor='\''$(call gb_Executable_get_target,cpp) -+ -DRC_INVOKED -DWINAPI_FAMILY=0 $(SOLARINC)'\' \
#	    && $(MAKE) \
#	)
#	$(call gb_Trace_EndRange,libgpg-error,EXTERNAL)
#else
#$(call gb_ExternalProject_get_state_target,libgpg-error,build):
#	$(call gb_Trace_StartRange,libgpg-error,EXTERNAL)
#	$(call gb_ExternalProject_run,build,\
#		MAKE=$(MAKE) ./configure \
#			--disable-rpath \
#			--disable-languages \
#			--disable-doc \
#			CPPFLAGS=" $(SOLARINC)" \
#			$(if $(filter MSC,$(COM)),--force_use_syscfg=true) \
#			$(if $(CROSS_COMPILING),--build=$(BUILD_PLATFORM) --host=$(HOST_PLATFORM)) \
#			$(if $(filter MACOSX,$(OS)),--prefix=/@.__________________________________________________OOO) \
#	                $(if $(filter TRUE,$(DISABLE_DYNLOADING)),--disable-shared,--disable-static) \
#	  && $(MAKE) \
#	)
#	$(call gb_Trace_EndRange,libgpg-error,EXTERNAL)
#
#endif



#$(eval $(call gb_UnpackedTarball_add_patches,libgpg-error, \
#	external/libgpg-error/libgpg-error_gawk5.patch \
#	$(if $(filter MSC,$(COM)),external/libgpg-error/w32-build-fixes.patch) \
#	$(if $(filter MSC,$(COM)),external/libgpg-error/w32-build-fixes-2.patch.1) \
#	$(if $(filter MSC,$(COM)),external/libgpg-error/w32-build-fixes-3.patch.1) \
#	$(if $(filter MSC,$(COM)),external/libgpg-error/w32-disable-dllinit.patch.1) \
#	external/libgpg-error/w32-build-fixes-4.patch \
#	external/libgpg-error/clang-cl.patch \
#	$(if $(filter LINUX,$(OS)),external/libgpg-error/libgpgerror-bundled-soname.patch.1) \
#))

class GetTextConan(ConanFile):
    name = "gpg-error"
    version = "1.27"
    description = "gpg-error"
    topics = ("conan", "gpg-error")
    url = "https://gitlab.com/hernad/conan-gpg-error"
    homepage = "https://gitlab.com/hernad/conan-gpg-error"
    license = "LGPL 2.1"
    settings = "os_build", "arch_build", "compiler"
    exports_sources = ["patches/*"]
    #requires = [("libiconv/1.16", "private")]

    _autotools = None

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    @property
    def _is_msvc(self):
        return self.settings.compiler == "Visual Studio"

    @property
    def _get_msvc_libs(self):
        libs = [ "ws2_32.lib", "shell32.lib" ]
        return libs

    @property
    def _get_msc_configure_flags(self):

        flags = [ "--enable-static",
			"--disable-shared",
			"--disable-rpath",
			"--disable-languages",
			"--disable-doc",
			"--disable-tests"
        ]
        return flags


    @property
    def _get_linux_configure_flags(self):

        flags = [ "--disable-static",
			"--disable-rpath",
			"--disable-doc",
			"--disable-languages",
			"--disable-doc",
			"--disable-tests"]
        return flags


    def configure(self):

        #del self.settings.compiler.libcxx
        #del self.settings.compiler.cppstd
        pass

    def build_requirements(self):

        if tools.os_info.is_windows:
            #self.build_requires("cygwin_installer/2.9.0@bincrafters/stable")
            self.build_requires("msys2/20190524")
            self.build_requires("automake/1.16.1")

        
        #if "CONAN_BASH_PATH" not in os.environ and tools.os_info.detect_windows_subsystem() != "msys2":
                #self.build_requires("msys2/20190524")
        #        self.build_requires("msys2/20161025")
        #if self._is_msvc:
        #    self.build_requires("automake/1.16.1")


    def source(self):
        tools.get(**self.conan_data["sources"][self.version])
        extracted_dir = "libgpg-error-" + self.version
        os.rename(extracted_dir, self._source_subfolder)

    def _configure_autotools(self):
        if self._autotools:
            return self._autotools

        build = None
        host = None
        rc = None
   
        if self._is_msvc:
            args = self._get_msc_configure_flags
            # INSTALL.windows: Native binaries, built using the MS Visual C/C++ tool chain.
            build = False
            if self.settings.arch_build == "x86":
                host = "i686-w64-mingw32"
                rc = "windres --target=pe-i386"
            elif self.settings.arch_build == "x86_64":
                host = "x86_64-w64-mingw32"
                rc = "windres --target=pe-x86-64"
            automake_perldir = tools.unix_path(os.path.join(self.deps_cpp_info['automake'].rootpath, "bin", "share", "automake-1.16"))

            args.extend(["CC=%s/compile cl -nologo" % automake_perldir,
                         "LD=link",
                         "NM=dumpbin -symbols",
                         "STRIP=:",
                         "AR=%s/ar-lib lib" % automake_perldir,
                         "RANLIB=:",
                         "--host=%s" % host])
            if rc:
                args.extend(['RC=%s' % rc, 'WINDRES=%s' % rc])
        else:
            args = self._get_linux_configure_flags
        
        self._autotools = AutoToolsBuildEnvironment(self, win_bash=tools.os_info.is_windows)

        if self._is_msvc:
            self._autotools.flags.append("-FS")


        self._autotools.configure(args=args, build=build, host=host)
        return self._autotools


    def build(self):
        for patch in self.conan_data["patches"][self.version]:
            tools.patch(**patch, fuzz=10)   
        with tools.vcvars(self.settings) if self._is_msvc else tools.no_op():
            with tools.environment_append(VisualStudioBuildEnvironment(self).vars) if self._is_msvc else tools.no_op():
                with tools.chdir(os.path.join(self._source_subfolder)):
                    #tools.replace_in_file( os.path.join("src", "mkheader.c"), "#include <unistd.h>", "#define R_OK 0x04" )
                    env_build = self._configure_autotools()
                    #text_file = open(os.path.join("src", "mkerrcodes.h"), "wt")
                    #text_file.write("xyz")
                    #text_file.close()
                    env_build.make(args = None)
                    


    def package(self):
        #self.copy(pattern="COPYING", dst="licenses", src=self._source_subfolder)

        # dist/public/nss, dist/public/dbm
        #self.copy(pattern="*", dst="include", src=os.path.join(self._source_subfolder, "dist", "public"))

        # dist/Release/include,bin,share,lib
        #self.copy(pattern="*", dst="", src=os.path.join(self._source_subfolder, "dist", "Release"))

        with tools.vcvars(self.settings) if self._is_msvc else tools.no_op():
            with tools.environment_append(VisualStudioBuildEnvironment(self).vars) if self._is_msvc else tools.no_op():
                with tools.chdir(os.path.join(self._source_subfolder)):
                    env_build = self._configure_autotools()
                    env_build.install()
        #tools.rmdir(os.path.join(self.package_folder, 'share'))
        #tools.rmdir(os.path.join(self.package_folder, 'lib'))
        #tools.rmdir(os.path.join(self.package_folder, 'include'))

    def package_id(self):
        self.info.include_build_settings()
        del self.info.settings.compiler

    def package_info(self):
        bindir = os.path.join(self.package_folder, "bin")
        self.output.info('Appending PATH environment variable: {}'.format(bindir))
        self.env_info.PATH.append(bindir)


